# Saham Edalat Price Watcher

This is a Price Watcher for Saham Edalat based on a scrapper :smiley:  
**Manual:**  
Download Chrome Driver from the link below according to your chrome version:  
[https://chromedriver.chromium.org/](https://chromedriver.chromium.org/)  
Extract the file and paste the chromedriver.exe file in `C:/chromedriver.exe`  
Install these four things:  
``` bash
	pip instal xlsxWriter
	pip install selenium
	pip install bs4
	pip install lxml
```
Enjoy :wink:
