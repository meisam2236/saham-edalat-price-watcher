import xlsxwriter
from bs4 import BeautifulSoup
from selenium  import webdriver
  
def replace_prices(text):
    return text.replace("٬","").replace("۰","0").replace("۱","1").replace("۲","2").replace("۳","3").replace("۴","4").replace("۵","5").replace("۶","6").replace("۷","7").replace("۸","8").replace("۹","9")

def replace_percentages(text):
    return text.replace("٪","").replace("٫",".").replace("٬",".").replace("۰","0").replace("۱","1").replace("۲","2").replace("۳","3").replace("۴","4").replace("۵","5").replace("۶","6").replace("۷","7").replace("۸","8").replace("۹","9")

workbook = xlsxwriter.Workbook('result.xls') 
driver = webdriver.Chrome('C:/chromedriver.exe')
sites = ['https://rahavard365.com/asset/703/%d9%81%d8%a7%d8%b1%d8%b3','https://rahavard365.com/asset/453/%d9%81%d9%88%d9%84%d8%a7%d8%af','https://rahavard365.com/asset/451/%d9%81%d9%85%d9%84%db%8c','https://rahavard365.com/asset/637/%d8%b4%d8%aa%d8%b1%d8%a7%d9%86','https://rahavard365.com/asset/462/%d9%88%d8%a8%d9%85%d9%84%d8%aa','https://rahavard365.com/asset/485/%d9%85%d8%a7%d8%b1%d9%88%d9%86','https://rahavard365.com/asset/489/%d8%a7%d8%ae%d8%a7%d8%a8%d8%b1','https://rahavard365.com/asset/442/%d8%b1%d9%85%d9%be%d9%86%d8%a7','https://rahavard365.com/asset/461/%d9%88%d8%aa%d8%ac%d8%a7%d8%b1%d8%aa','https://rahavard365.com/asset/382/%da%a9%da%86%d8%a7%d8%af','https://rahavard365.com/asset/455/%d9%81%d8%ae%d9%88%d8%b2','https://rahavard365.com/asset/498/%d9%88%d8%a8%d8%b5%d8%a7%d8%af%d8%b1','https://rahavard365.com/asset/481/%d8%ad%da%a9%d8%b4%d8%aa%db%8c','https://rahavard365.com/asset/484/%d8%b4%d9%be%d9%86%d8%a7','https://rahavard365.com/asset/591/%d8%b4%d8%a8%d9%86%d8%af%d8%b1','https://rahavard365.com/asset/419/%da%a9%da%af%d9%84','https://rahavard365.com/asset/712/%d8%ac%d9%85','https://rahavard365.com/asset/501/%d8%a8%d9%81%d8%ac%d8%b1','https://rahavard365.com/asset/492/%d8%b4%d8%a8%d8%b1%db%8c%d8%b2','https://rahavard365.com/asset/569/%d9%81%d9%88%d9%84%d8%a7%da%98','https://rahavard365.com/asset/366/%d8%a8%d8%b3%d9%88%db%8c%da%86','https://rahavard365.com/asset/1192/%da%a9%da%af%d9%84%d8%ad','https://rahavard365.com/asset/240/%d9%81%d8%a7%db%8c%d8%b1%d8%a7','https://rahavard365.com/asset/7801/%d8%b4%da%af%d9%88%db%8c%d8%a7','https://rahavard365.com/asset/502/%d8%af%d8%a7%d9%86%d8%a7','https://rahavard365.com/asset/710/%d8%b4%d8%a7%d9%88%d8%a7%d9%86','https://rahavard365.com/asset/696/%d8%b4%d8%b1%d8%a7%d8%b2','https://rahavard365.com/asset/454/%d8%b3%d8%af%d8%b4%d8%aa','https://rahavard365.com/asset/604/%d9%88%d9%be%d8%b3%d8%aa','https://rahavard365.com/asset/421/%d9%84%da%a9%d9%85%d8%a7','https://rahavard365.com/asset/316/%d9%88%d8%aa%d9%88%d8%b5%d8%a7','https://rahavard365.com/asset/390/%d8%a8%d9%86%db%8c%d8%b1%d9%88','https://rahavard365.com/asset/304/%d8%b3%d8%a7%d8%b1%d8%a7%d8%a8','https://rahavard365.com/asset/167/%d8%ae%d9%88%d8%af%d8%b1%d9%88','https://rahavard365.com/asset/172/%d8%ae%d8%b3%d8%a7%d9%be%d8%a7','https://rahavard365.com/asset/380/%d8%ad%d9%be%d8%aa%d8%b1%d9%88','https://rahavard365.com/asset/57/%d9%88%d8%b1%d9%86%d8%a7']
number = [1146, 1260, 747, 615, 299, 47, 239, 137, 1788, 181, 114, 1400, 78, 203, 114, 104, 41, 38, 39, 47, 9, 57, 31, 47, 43, 4, 4, 8, 16, 22, 12, 3, 8, 47, 39, 1, 3]
balance = 0
worksheet = workbook.add_worksheet()

worksheet.write('A1', 'نام نماد') 
worksheet.write('B1', 'تعداد') 
worksheet.write('C1', 'درصد پایانی') 
worksheet.write('D1', 'قیمت پایانی') 
worksheet.write('E1', 'موجودی') 
num = 2

for i in range(len(sites)):
    driver.get(sites[i])
    soup = BeautifulSoup(driver.page_source,"lxml")
    name = soup.select('.asset-symbol')[0].text.strip()
    final_percentage = soup.select('.symbolprices')[0].select("span")[4].text
    final_price = soup.select('.symbolprices')[0].select("span")[0].text
    stock = int(replace_prices(final_price))*number[i]
    
    print("نام نماد: "+ name)
    worksheet.write('A'+str(num), name)
    print("تعداد: "+ str(number[i]))
    worksheet.write('B'+str(num), number[i]) 
    print("درصد پایانی: "+ replace_percentages(final_percentage))
    worksheet.write('C'+str(num), float(replace_percentages(final_percentage))) 
    print("قیمت پایانی: "+ replace_prices(final_price))
    worksheet.write('D'+str(num), int(replace_prices(final_price))) 
    print("موجودی: "+ str(stock))
    worksheet.write('E'+str(num), stock) 
    balance = balance + stock
    print("\n")
    num = num + 1

print("موجودی کل: " + str(balance))
worksheet.write('E'+str(num), balance)

workbook.close()
driver.quit()